import React from 'react';
import CardPage from '../card_page/CardPage.js';
import Nav from '../nav/nav.jsx';

import './app.css';
import Search from "../Search/search";

const App = () => {
    return (
        <div className='main'>
            <div className="box">
                <Search />
                <h1>Люди</h1>
                <Nav></Nav>
                <section>
                    <CardPage/>
                </section>
            </div>
        </div>
    )
}

export default App;