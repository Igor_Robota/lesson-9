import React from 'react';

import './nav-item.css';

const NavItem = ({name}) => {
    return (
        <li className='piple__list'><span>{name}</span> </li>
    )
}
export default NavItem;