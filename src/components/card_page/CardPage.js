import React, { Component } from "react";
import { request } from "../../service/data";
import Card from "./Element-card/element-card";
import uniqid from 'uniqid';

import './card-page.css'

class CardPage extends Component {
    state ={
        data: [],
        loader: false
    }

    componentDidMount(){
        console.log('componentDidMount');
        request('https://swapi.dev/api/people/')
            .then( (res) => {
                this.setState( (state) => {
                    return {
                        data: res,
                        loader: true
                    }
                })
            })
    }

    componentDidUpdate() {
        // console.log('componentDidUpdate', this.state.data);
    }

    componentWillUnmount() {
        // console.log('componentWillUnmount');
    }
    
    componentDidCatch() {
        // console.log('componentDidCatch');
    }

    render = () => {

        const { data, loader } = this.state;

        return(
            <>
                <div className="card-page">

                    {   loader?
                        data.results.map(e => {
                            return(
                                <Card key={uniqid()} img={null} name={e.name} gender={e.gender} birth_year={e.birth_year}/>
                            )
                            
                        }):
                        <></>
                    }
                </div>

            </>
        );
    };
};

export default CardPage;
