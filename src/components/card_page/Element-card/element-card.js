import React from "react";
import Button from 'react-bootstrap/Button'
import Card from "react-bootstrap/Card";

import './element-card.css'

const ElementCard = ({ img, name, gender, birth_year }) => {
    return(
        <Card className="element-card">
            <Card.Img variant="top" src="https://img05.rl0.ru/afisha/e750x420p0x0f1024x512q85i/s1.afisha.ru/mediastorage/8c/43/f69add78fae94f32869625d7438c.jpg" />
            <Card.Body>
                <Card.Title>Peaple info</Card.Title>
                    <ul>
                        <li>Имя:{name}</li>
                        <li>Пол: {gender}</li>
                        <li>День рождения: {birth_year}</li>
                    </ul>
                <Button variant="primary">Подробней</Button>
            </Card.Body>
        </Card>
    );
};

export default ElementCard;
