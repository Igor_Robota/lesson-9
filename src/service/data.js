// function request() {
//     const xhr = new XMLHttpRequest();
//     xhr.open('GET', 'https://swapi.dev/api/');
//     xhr.onreadystatechange = function() {
//         if(xhr.readyState === 4 && xhr.status === 200) {
//             return xhr.responseText;
//         }
//     }
//     xhr.send();
// } 

// export default fetch('https://swapi.dev/api/').then((value) =>{
//     return value.json()
// }).then((value) => {
//     return value
// }).catch((error)=>{
//     console.error(error)
// })

export const request = async (url) => {
    const data = await fetch(url)
        return data.json()
}
