import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app/app.jsx';

import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(<App></App>, document.querySelector('#root'));